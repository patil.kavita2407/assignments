# Initializing list
myList = [10, [], 25, 73, [], 94, 35, [], [], 89]

# printing original list
print("The original list is : " + str(myList))

# Remove empty List from List
result = list(filter(None, myList))

# printing result
print ("List after empty list removal : " + str(result))